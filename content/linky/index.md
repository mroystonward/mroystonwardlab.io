---
url: /linky/
type: _default
layout: blank
---
<head>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Murray Royston-Ward</title>
<style>
body {margin:0; background-color: #1C1C1C; color: #fff; font-size: 1rem; font-weight: 600; line-height: 1.25; display: block; font-family: 'Karla', sans-serif; width: 100%; text-align: center; text-decoration: none;}
.bg {animation:slide 3s ease-in-out infinite alternate; background-image: linear-gradient(-60deg, #FCDF33 50%, #51BFC7 50%); bottom:0; left:-20%; opacity:.5; position:fixed; right:-20%; top:0; z-index:-1;}
.bg2 {animation-direction:alternate-reverse; animation-duration:4s;}
.bg3 {animation-duration:5s;}
@keyframes slide {0% {transform:translateX(-25%);} 100% {transform:translateX(25%);}}
ul {max-width: 675px; width: auto; display: block; margin: 27px auto; padding: 20px;}
li {display: block; background-color: #E6E6E6; text-align: center; margin-bottom: 20px; padding: 17px; transition: all .25s cubic-bezier(.08,.59,.29,.99); border: solid #E6E6E6 2px;}
li a {color: #1C1C1C; font-family: 'Karla', sans-serif; text-decoration: none; font-size: 1rem;}
li:hover {background-color: #1C1C1C; color: #E6E6E6;}
li:hover a {color: inherit;}
#userPhoto {width: 110px; height: 110px; display: block; margin: 35px auto 20px; border-radius: 50%;}
</style>
</head>

<div class="bg"></div>
<div class="bg bg2"></div>
<div class="bg bg3"></div>
<img id="userPhoto" src="bio-photo.jpg" alt="User Photo">

improvised music, DIY electronics, workshops, etc.

- [The House Organ](https://thehouseorgan.xyz/)
- [Portfolio](https://mroystonward.com/)
- [Tripping on Wires](https://trippingonwires.mroystonward.com)
- [Bandcamp](https://thehouseorgan.bandcamp.com/)
